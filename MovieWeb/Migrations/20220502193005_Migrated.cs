﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieWeb.Migrations
{
    public partial class Migrated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MovieNights",
                columns: table => new
                {
                    MovieNightId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Omschrijving = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieNights", x => x.MovieNightId);
                });

            migrationBuilder.CreateTable(
                name: "RentedBy",
                columns: table => new
                {
                    RentedById = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    MovieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RentedBy", x => x.RentedById);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Voornaam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MovieNightId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerId);
                    table.ForeignKey(
                        name: "FK_Customers_MovieNights_MovieNightId",
                        column: x => x.MovieNightId,
                        principalTable: "MovieNights",
                        principalColumn: "MovieNightId");
                });

            migrationBuilder.CreateTable(
                name: "MovieImg",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MovieId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieImg", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Tittel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Duurtijd = table.Column<int>(type: "int", nullable: false),
                    Release = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MovieImgId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    MovieNightId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_MovieImg_MovieImgId",
                        column: x => x.MovieImgId,
                        principalTable: "MovieImg",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Movies_MovieNights_MovieNightId",
                        column: x => x.MovieNightId,
                        principalTable: "MovieNights",
                        principalColumn: "MovieNightId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_MovieNightId",
                table: "Customers",
                column: "MovieNightId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieImg_MovieId",
                table: "MovieImg",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_MovieImgId",
                table: "Movies",
                column: "MovieImgId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_MovieNightId",
                table: "Movies",
                column: "MovieNightId");

            migrationBuilder.AddForeignKey(
                name: "FK_MovieImg_Movies_MovieId",
                table: "MovieImg",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_MovieNights_MovieNightId",
                table: "Movies");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieImg_Movies_MovieId",
                table: "MovieImg");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "RentedBy");

            migrationBuilder.DropTable(
                name: "MovieNights");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "MovieImg");
        }
    }
}
