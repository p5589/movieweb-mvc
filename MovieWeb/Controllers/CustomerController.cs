﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieWeb.Database.Design;
using MovieWeb.Domain;
using MovieWeb.Models;

namespace MovieWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CustomerController : Controller
    {
        private IMovieDatabase _context;


        public CustomerController(IMovieDatabase context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

        }
        public IActionResult Index()
        {
            List<CustomerListViewModel> Customers = new List<CustomerListViewModel>();

            foreach (var Customer in _context.GetCustomer())
            {
                Customers.Add(new CustomerListViewModel() { CustomerId = Customer.CustomerId, Naam = Customer.Naam, Voornaam = Customer.Voornaam });
            }
            return View(Customers);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CustomerCreateViewModel customerCreateViewModel)
        {           
            var isModelValid = TryValidateModel(customerCreateViewModel);

            if (isModelValid)
            {
                var newEntity = new Customer
                {
                    CustomerId = customerCreateViewModel.CustomerId,
                    Naam = customerCreateViewModel.Naam,
                    Voornaam = customerCreateViewModel.Voornaam

                };
                
                _context.AddCustomer(newEntity);

                return RedirectToAction("Index");
            }
            return View(customerCreateViewModel);
        }
        [HttpGet]
        public IActionResult Create()
        {
            var customerCreateViewModel = new CustomerCreateViewModel();

            return View(customerCreateViewModel);
        }


        [HttpGet]
        public IActionResult Detail(Guid CustomerId)
        {         
            var selectedCustomer = _context.FindCustomerById(CustomerId);
            CustomerDetailViewModel viewCustomer = new CustomerDetailViewModel()
            {
                Naam = selectedCustomer.Naam,
                Voornaam = selectedCustomer.Voornaam,
            };
            return View(viewCustomer);
        }


        [HttpGet]
        public IActionResult Edit(Guid CustomerId)
        {
            var selectedCustomer = _context.FindCustomerById(CustomerId);

            if (selectedCustomer == null) return new NotFoundResult();

            var newCustomerEdit = new CustomerEditViewModel()
            {
                Naam = selectedCustomer.Naam,
                Voornaam = selectedCustomer.Voornaam,
            };

            return View(newCustomerEdit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid Id, [FromForm] CustomerEditViewModel newCustomerEdit)
        {
            var selectedCustomer = _context.FindCustomerById(newCustomerEdit.CustomerId);

            if (selectedCustomer == null) return new NotFoundResult();

            selectedCustomer.Naam = newCustomerEdit.Naam;
            selectedCustomer.Voornaam = newCustomerEdit.Voornaam;

            _context.UpdateCustomer(selectedCustomer);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(Guid customerId)
        {
            var selectedCustomer = _context.FindCustomerById(customerId);

            var newCustomerDelete = new CustomerDeleteViewModel()
            {
                CustomerId = selectedCustomer.CustomerId,
                Naam = selectedCustomer.Naam,
                
            };

            return View(newCustomerDelete);
        }
        [HttpPost]
        public IActionResult DeleteConfirm(CustomerDeleteViewModel newCustomerDelete)
        {            
            var selectedCustomer = _context.FindCustomerById(newCustomerDelete.CustomerId);

            if (selectedCustomer == null) return new NotFoundResult();

            _context.RemoveCustomer(selectedCustomer);

            return RedirectToAction("Index");
        }
    }
}

