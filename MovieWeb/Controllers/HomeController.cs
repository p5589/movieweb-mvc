﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieWeb.Models;
using System.Diagnostics;
using System.Security.Claims;

namespace MovieWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {         
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
       

        [HttpGet]
        public IActionResult Fibonacci()
        {

            var fiboViewModel = new FibonacciViewModel();
            return View(fiboViewModel);
        }
        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult Fibonacci(FibonacciViewModel fibonacciViewModel)
        {           
            return View(fibonacciViewModel);
        }
        
        public IActionResult Error()
        {

            var errorViewModel = new ErrorViewModel()
            {
                Message = "NIET DEV ENVIRONMENT DETECTED!!"
            };
            return View(errorViewModel);
        }
        
        public IActionResult Secured()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpGet("login")]
        public IActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task <IActionResult> Validate(string gebruikersnaam, string paswoord, string returnUrl )
        {
           if(gebruikersnaam == "Bertje" && paswoord == "Bakker")
            {
                var claims = new List<Claim>();
                claims.Add(new Claim("gebruikersnaam", gebruikersnaam));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, gebruikersnaam));
                claims.Add(new Claim(ClaimTypes.Name, gebruikersnaam));
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));
                var claimsIdentity = new ClaimsIdentity(claims, Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await HttpContext.SignInAsync(claimsPrincipal);
                return Redirect(returnUrl);
            }
            TempData["Error"] = "Gebruikersnaam en/of paswoord is ongeldig";
            ViewData["ReturnUrl"] = returnUrl;
            return View("login");
        }
        [AllowAnonymous]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }
        [AllowAnonymous]
        [HttpGet("denied")]
        public IActionResult Denied(string returnUrl)
        {
            return View();
        }
    }

}