﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieWeb.Database.Design;
using MovieWeb.Domain;
using MovieWeb.Models;

namespace MovieWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MovieNightController : Controller
    {
        private IMovieDatabase _context;
        public static List<Customer> allCustomers = new List<Customer>();
        public static List<Movie> allMovies = new List<Movie>();
        public static Customer customerToAdd;
        public static Movie movieToAdd;

        public MovieNightController(IMovieDatabase context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            allCustomers = _context.GetCustomer();
            allMovies = _context.GetMovies();
        }
        public IActionResult Index()
        {
            List<MovieNightCreateViewModel> MovieNights = new List<MovieNightCreateViewModel>();

            foreach (var MovieNight in _context.GetMovieNight())
            {
                MovieNights.Add(new MovieNightCreateViewModel() { MovieNightId = MovieNight.MovieNightId, Naam = MovieNight.Naam, Omschrijving = MovieNight.Omschrijving, AllCustomers = MovieNight.AllCustomers });
            }
            return View(MovieNights);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var movieNightCreateViewModel = new MovieNightCreateViewModel();

            return View(movieNightCreateViewModel);
        }
              

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(MovieNightCreateViewModel MovieNightCreateViewModel)
        {
            
            var isModelValid = TryValidateModel(MovieNightCreateViewModel);

            if (isModelValid)
            {
                           
                
                
                    var newEntity = new MovieNight
                    {
                        MovieNightId = Guid.NewGuid(),
                        Naam = MovieNightCreateViewModel.Naam,
                        Omschrijving = MovieNightCreateViewModel.Omschrijving,
                        AllCustomers = new List<Customer>(),
                        AllMovies = new List<Movie>()
                    };
                    _context.AddMovieNight(newEntity);
                    return RedirectToAction("Index");
                 
            }
            return View(MovieNightCreateViewModel);
        }          
        [HttpGet]
        public IActionResult Detail(Guid MovieNightId)
        {           
            var selectedMovieNight = _context.FindMovieNightById(MovieNightId);
            MovieNightCreateViewModel viewMovieNight = new MovieNightCreateViewModel()
            {
                Naam = selectedMovieNight.Naam,
                
            };
            return View(viewMovieNight);
        }


        [HttpGet]
        public IActionResult Edit(Guid MovieNightId)
        {
            var selectedMovieNight = _context.FindMovieNightById(MovieNightId);

            if (selectedMovieNight == null) return new NotFoundResult();

            var newMovieNightEdit = new MovieNightCreateViewModel()
            {
                MovieNightId = selectedMovieNight.MovieNightId,
                Naam = selectedMovieNight.Naam,
                Omschrijving = selectedMovieNight.Omschrijving
                
                
            };
            if (selectedMovieNight.AllCustomers != null)
            {
                newMovieNightEdit.AllCustomers = selectedMovieNight.AllCustomers.ToList();
            }
            return View(newMovieNightEdit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid Id, [FromForm] MovieNightCreateViewModel newMovieNightEdit)
        {
            var selectedMovieNight = _context.FindMovieNightById(newMovieNightEdit.MovieNightId);

            if (selectedMovieNight == null) return new NotFoundResult();

            selectedMovieNight.Naam = newMovieNightEdit.Naam;
            

            _context.UpdateMovieNight(selectedMovieNight);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(Guid MovieNightId)
        {
            var selectedMovieNight = _context.FindMovieNightById(MovieNightId);

            var newMovieNightDelete = new MovieNightCreateViewModel()
            {
                MovieNightId = selectedMovieNight.MovieNightId,
                Naam = selectedMovieNight.Naam
            };

            return View(newMovieNightDelete);
        }
        [HttpPost]
        public IActionResult DeleteConfirm(MovieCreateViewModel newMovieNightDelete)
        {           
            var selectedMovieNight = _context.FindMovieNightById(newMovieNightDelete.MovieId);

            if (selectedMovieNight == null) return new NotFoundResult();

            _context.RemoveMovieNight(selectedMovieNight);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult AddCustomer(Guid MovieNightId)
        {
            var selectedMovieNight = _context.FindMovieNightById(MovieNightId);
            var newMovieNightAddCustomer = new MovieNightCreateViewModel()
            {
                MovieNightId = selectedMovieNight.MovieNightId,
                Naam = selectedMovieNight.Naam,
                Omschrijving = selectedMovieNight.Omschrijving,
                
            };
            if (selectedMovieNight.AllCustomers != null)
            {
                newMovieNightAddCustomer.AllCustomers = selectedMovieNight.AllCustomers.ToList();
            }
            return View(newMovieNightAddCustomer);          
        }
        [HttpPost]
        public IActionResult AddCustomer(MovieNightCreateViewModel newMovieNightAddCustomer)
        {
            var selectedCustomer = _context.FindCustomerById(newMovieNightAddCustomer.CustomerToAdd.CustomerId);
            var selectedMovieNight = _context.FindMovieNightById(newMovieNightAddCustomer.MovieNightId);
            if (selectedCustomer == null) return new NotFoundResult();
            if(selectedMovieNight.AllCustomers == null)
            {
                selectedMovieNight.AllCustomers = new List<Customer>();
            }
            selectedMovieNight.AllCustomers.Add(selectedCustomer);
           _context.UpdateMovieNight(selectedMovieNight);
            return RedirectToAction("Index");
        }
    }
}
