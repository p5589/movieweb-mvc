﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieWeb.Database;
using MovieWeb.Database.Design;
using MovieWeb.Domain;
using MovieWeb.Models;
using SpelenMetBlazor.Data;

namespace MovieWeb.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class MovieController : Controller
    {
        private IMovieDatabase _myDatabase;

        private static List<Movie> movieEntities = new List<Movie>();
        
        private static List<MovieImg> movieImgEntities = new List<MovieImg>();

        private static MovieImg ImgToUpload;

        public static int Filter = 0;

        public static string Search = "";

        public static List<ExampleModel> mylist;
   
        public MovieController(IMovieDatabase mydatabase)
        {
            _myDatabase = mydatabase;
            movieEntities = _myDatabase.GetMovies();
            movieImgEntities = _myDatabase.GetMovieImg();          
        }
                      
        [HttpGet]        
        public IActionResult Index()
        {
            List<Movie> mylist;
            var movieViewModels = new List<MovieListViewModel>();                     
            if (Search != "")
            {   
                    movieEntities = movieEntities.FindAll(x => x.Tittel.Contains($"{Search}"));
                Search = "";
            }
            switch (Filter)
            {
                case 1:
                    mylist = movieEntities.OrderByDescending(x => x.Tittel).ToList();
                    break;
                case 2:
                    mylist = movieEntities.OrderByDescending(x => x.Release).ToList();
                    break;
                case 3:
                    mylist = movieEntities.OrderBy(x => x.Release).ToList();
                    break;
                default:
                    mylist = movieEntities.OrderBy(x => x.Tittel).ToList();
                    break;

            }
            foreach (Movie movie in mylist)
            {
                movieViewModels.Add(new MovieListViewModel() { MovieId = movie.MovieId, Duurtijd = movie.Duurtijd, Tittel = movie.Tittel, MovieImg = movie.MovieImg, Release = movie.Release });
            }
            return View(movieViewModels);
        }
        [HttpGet]
        public IActionResult Create()
        {
            var movieCreateViewModel = new MovieCreateViewModel();
            if (ImgToUpload != null)
            {
                movieCreateViewModel.MovieImg = ImgToUpload;               
            }

            return View(movieCreateViewModel);
        }
        [HttpGet]
        public IActionResult DeleteImg(MovieImg myimage)
        {
            movieImgEntities.Clear();
            return RedirectToAction("Create");
        }
        
        [AutoValidateAntiforgeryToken]
        [HttpPost]       
        public IActionResult Create(MovieCreateViewModel movieViewModel)
        {
            var isValid = TryValidateModel(movieViewModel);
            
            if (isValid)
            {
                var newEntity = new Movie
                {
                    Tittel = movieViewModel.Tittel,           
                    Duurtijd = movieViewModel.Duurtijd,
                    MovieId = Guid.NewGuid(),
                    Release = movieViewModel.Release,
                    MovieImg = new MovieImg() { Id = ImgToUpload.Id, Path = ImgToUpload.Path },

                };
                movieEntities.Add(newEntity);
                _myDatabase.AddMovie(newEntity);
                ImgToUpload = null;
                return RedirectToAction("Index");
            }
            else if (movieImgEntities.Count != 0)
            {
                movieViewModel.MovieImg = new MovieImg() { Id = ImgToUpload.Id, Path = ImgToUpload.Path };
                ImgToUpload = null;
            }
            return View(movieViewModel);
        }
        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult Detail()
        {
            MovieDetailViewModel mijnModel = new MovieDetailViewModel();
            string myvalue = Request.Form["LijstFilms"].ToString();
            if (myvalue == "")
                myvalue = Request.Form["Lijst"].ToString();
            if (myvalue != "")
            {
                foreach (Movie movie in movieEntities)
                {
                    if (movie.Tittel == myvalue)
                    {
                        mijnModel.MovieId = movie.MovieId;
                        mijnModel.Tittel = movie.Tittel;
                        mijnModel.Duurtijd = movie.Duurtijd;
                        mijnModel.Release = movie.Release;
                        mijnModel.MovieImg = movie.MovieImg;
                    }
                }
                return View(mijnModel);
            }          
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Detail(Guid sausissen)
        {
            
            var mymovie = movieEntities.FirstOrDefault(x => x.MovieId == sausissen);
            var movieDetailViewModel = new MovieDetailViewModel()
            {
                Tittel = mymovie.Tittel,
                MovieId = mymovie.MovieId,
                MovieImg = mymovie.MovieImg,
                Duurtijd = mymovie.Duurtijd,
                Release = mymovie.Release,
            };
            return View(movieDetailViewModel);
        }
        [HttpGet]
        public IActionResult CreateImg(MovieCreateViewModel model)
        {
            var isModelValid = TryValidateModel(model);
            MovieCreateViewModel thismodel = new MovieCreateViewModel();
            if (isModelValid)
            {
                thismodel.MovieImg = model.MovieImg;
                thismodel.MovieId = model.MovieId;
                thismodel.Tittel = model.Tittel;
                thismodel.Duurtijd = model.Duurtijd;
                thismodel.Release = model.Release;

            }
            return View(thismodel);
        }

        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public async Task<IActionResult> CreateImg(MovieImg image, IFormFile uploadFile)
        {           
            if (uploadFile != null && uploadFile.Length > 0)
            {
                var fileName = Path.GetFileName(uploadFile.FileName);

                var filePath = Path.Combine(@"wwwroot\Images\", fileName);
                string[] subbie = filePath.Split('\\');
                //"~/Images/angularmerkel.png"
                image.Id = Guid.NewGuid(); 
                image.Path = "~/" + subbie[1] + "/" + subbie[2];
                movieImgEntities.Add(image);
                _myDatabase.AddMovieImg(image);
                ImgToUpload = image;
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await uploadFile.CopyToAsync(fileStream);
                    
                }

            }
            return RedirectToAction("Create");
        }

        [HttpGet]
        public IActionResult Update(Guid id)
        {
            MovieCreateViewModel movieCreateViewModel = new MovieCreateViewModel();
            foreach (var movie in movieEntities)
            {
                if (movie.MovieId == id)
                {
                    movieCreateViewModel.Tittel = movie.Tittel;
                    movieCreateViewModel.MovieImg = movie.MovieImg;
                    movieCreateViewModel.Duurtijd = movie.Duurtijd;
                    movieCreateViewModel.MovieId = id;
                    movieCreateViewModel.MovieScreenshots = movie.MovieScreenshots;
                    movieCreateViewModel.Release = movie.Release;
                    break;
                }
            }
            return View(movieCreateViewModel);
        }
        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult Update(MovieCreateViewModel movieViewModel)
        {

            Movie mymovie = movieEntities.FirstOrDefault(x => x.MovieId == movieViewModel.MovieId);
            ImgToUpload = mymovie.MovieImg;
            movieEntities.Remove(movieEntities.FirstOrDefault(x => x.MovieId == movieViewModel.MovieId));
                var newEntity = new Movie
                {                   
                    
                    Tittel = movieViewModel.Tittel,
                    Duurtijd = movieViewModel.Duurtijd,
                    MovieId = movieViewModel.MovieId,
                    Release = movieViewModel.Release,
                    MovieImg = ImgToUpload,
                };
                movieEntities.Add(newEntity);
                _myDatabase.UpdateMovie(newEntity);
                return RedirectToAction("Index");           
        }
        [HttpGet]
        public IActionResult Delete(Guid id)
        {          
            var movie = movieEntities.FirstOrDefault(x => x.MovieId == id);
            MovieCreateViewModel movieCreateViewModel = new MovieCreateViewModel
            {
                Tittel = movie.Tittel,
                MovieId = movie.MovieId,
                MovieImg = movie.MovieImg,
                Duurtijd = movie.Duurtijd,
                Release = movie.Release,
            };
            return View(movieCreateViewModel);
        }
        public IActionResult DeleteConfirm(Guid id)
        {
            var movie = movieEntities.FirstOrDefault(x => x.MovieId == id);
            movieEntities.Remove(movie);
            _myDatabase.RemoveMovie(movie);
            return RedirectToAction("Index");
        }
      
        public IActionResult IndexSort(int filter)
        {
            Filter = filter;


            return RedirectToAction("Index");
        }
        
        public IActionResult SearchTitle(string search)
        {
             Search = search;

            return RedirectToAction("Index");
        }

        public IActionResult Forms()
        {          
            return View();
        }
    }
}
