﻿using Microsoft.AspNetCore.Mvc;
using MovieWeb.Domain;
using MovieWeb.Models;

namespace MovieWeb.Controllers
{
    public class StatisticsController : Controller
    {
        public static List<Uitgave> lijstUitgaven = new List<Uitgave>
        {
            new Uitgave() {ID = Guid.NewGuid(), Description = "original invoice", Date = DateTime.Now, Amount = 50}
        };

        public static int Filter = 0;
        [HttpGet]
        public IActionResult Index()
        {
            List<UitgaveDetailViewModel> lijstUitgave = new List<UitgaveDetailViewModel>();
            foreach (var item in lijstUitgaven)
            {
                lijstUitgave.Add(new UitgaveDetailViewModel()
                {
                    ID = item.ID,
                    Description = item.Description,
                    Date = item.Date,
                    Amount = item.Amount
                });
            }

            switch(Filter)
            {
                case 1:
                    lijstUitgave.OrderBy(x => x.Date);
                    break;
                case 2:
                    lijstUitgave.OrderByDescending(x => x.Amount);
                    break;
                case 3:
                    lijstUitgave.OrderBy(x => x.Amount);
                    break;
                default:
                    lijstUitgave.OrderByDescending(x => x.Date);
                    break;

            }
            return View(lijstUitgave);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult IndexSort(int filter)
        {
            Filter = filter;

        
            return RedirectToAction("Index");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult IndexReset()
        {
            Filter = 0;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Create()
        {
            var uitgaveEntity = new UitgaveDetailViewModel();
         
            return View(uitgaveEntity);
        }

        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult Create(UitgaveDetailViewModel dmodel)
        {
            if(TryValidateModel(dmodel))
            {
                var newuitgave = new Uitgave
                {
                    ID = Guid.NewGuid(),
                    Description = dmodel.Description,
                    Amount = dmodel.Amount,
                    Date = dmodel.Date
                };
                lijstUitgaven.Add(newuitgave);
                return RedirectToAction("Index");
            }
            return View(dmodel);
        }
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var y = lijstUitgaven.SingleOrDefault(x => x.ID == id);

            var dmodel = new UitgaveDetailViewModel
            {
                ID = y.ID,
                Description = y.Description,    
                Date = y.Date,
                Amount = y.Amount
            };
            return View(dmodel);
        }
        
        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult Edit(Guid id, UitgaveDetailViewModel dmodel)
        {
            if(TryValidateModel(dmodel))
            {
                var y = lijstUitgaven.SingleOrDefault(x => x.ID == dmodel.ID);
                y.Description = dmodel.Description;
                y.Amount = dmodel.Amount;
                y.Date = dmodel.Date;
                
                return RedirectToAction("Index");
            }
            return View(dmodel);
        }
        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var y = lijstUitgaven.SingleOrDefault(x => x.ID == id);

            var dmodel = new UitgaveDeleteViewModel
            {
                ID = y.ID,
                Description = y.Description,
                Date = y.Date,
            };
            return View(dmodel);
        }
        [AutoValidateAntiforgeryToken]
        [HttpPost]
        public IActionResult DeleteConfirm(Guid id)
        {

                var y = lijstUitgaven.Single(x => x.ID == id);
               lijstUitgaven.Remove(y);

                return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Statistics()
        {
            List<UitgaveDetailViewModel> lijstUitgave = new List<UitgaveDetailViewModel>();
            foreach (var item in lijstUitgaven)
            {
                lijstUitgave.Add(new UitgaveDetailViewModel()
                {
                    ID = item.ID,
                    Description = item.Description,
                    Date = item.Date,
                    Amount = item.Amount
                });
            }

            return View(lijstUitgave);
        }
    }
    
}

