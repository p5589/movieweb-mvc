﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieWeb.Database.Design;
using MovieWeb.Domain;
using MovieWeb.Models;

namespace MovieWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RentedByController : Controller
    {
        private IMovieDatabase _context;
        public static List<Customer> allCustomers = new List<Customer>();
        public static List<Movie> allMovies = new List<Movie>();
        public static List<RentedBy> rentedByEnteties;

        public RentedByController(IMovieDatabase context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            allCustomers = _context.GetCustomer();
            allMovies = _context.GetMovies();
            rentedByEnteties = _context.GetRentedBy();
        }
        public IActionResult Index()
        {
            List<RentedByListViewModel> RentedBys = new List<RentedByListViewModel>();


            foreach (var RentedBy in rentedByEnteties)
            {
                RentedBys.Add(new RentedByListViewModel() { RentedById = RentedBy.RentedById, Customer = _context.FindCustomerById(RentedBy.CustomerId), Movie = _context.FindMovieByID(RentedBy.MovieId) });
            }
            return View(RentedBys);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(RentedByCreateViewModel RentedByCreateViewModel)
        {
            //Controleer of de parameter geldig is:
            var isModelValid = TryValidateModel(RentedByCreateViewModel);

            if (isModelValid)
            {
                var newEntity = new RentedBy
                {
                    RentedById = RentedByCreateViewModel.RentedById,
                    CustomerId = RentedByCreateViewModel.CustomerId,
                    MovieId = RentedByCreateViewModel.MovieId,
                };

                _context.AddRentedBy(newEntity);

                return RedirectToAction("Index");
            }
            return View(RentedByCreateViewModel);
        }
        [HttpGet]
        public IActionResult Create()
        {
            var RentedByCreateViewModel = new RentedByCreateViewModel();



            return View(RentedByCreateViewModel);
        }


        [HttpGet]
        public IActionResult Detail(Guid RentedById)
        {
            //Movie selectedMovie = movieEntities.FirstOrDefault(x => x.Title == title);

            var selectedRentedBy = _context.FindRentedById(RentedById);
            RentedByDetailViewModel viewRentedBy = new RentedByDetailViewModel()
            {
                RentedById = selectedRentedBy.RentedById,
                Customer = _context.FindCustomerById(selectedRentedBy.CustomerId),
                Movie = _context.FindMovieByID(selectedRentedBy.MovieId),
            };
            return View(viewRentedBy);
        }


        [HttpGet]
        public IActionResult Edit(Guid RentedById)
        {
            var selectedRentedBy = _context.FindRentedById(RentedById);

            if (selectedRentedBy == null) return new NotFoundResult();

            var newRentedByEdit = new RentedByEditViewModel()
            {
                RentedById = selectedRentedBy.RentedById,
                CustomerId = selectedRentedBy.CustomerId,
                MovieId = selectedRentedBy.MovieId
            };

            return View(newRentedByEdit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Guid RentedById, [FromForm] RentedByEditViewModel newRentedByEdit)
        {
            var selectedRentedBy = _context.FindRentedById(newRentedByEdit.RentedById);

            var modelValid = TryValidateModel(newRentedByEdit);

            if (modelValid)
            {
                if (selectedRentedBy == null) return new NotFoundResult();

                selectedRentedBy.CustomerId = newRentedByEdit.CustomerId;
                selectedRentedBy.MovieId = newRentedByEdit.MovieId;

                _context.UpdateRentedBy(selectedRentedBy);

                return RedirectToAction("Index");
            }
            return View(newRentedByEdit);
        }

        [HttpGet]
        public IActionResult Delete(Guid RentedById)
        {
            var selectedRentedBy = _context.FindRentedById(RentedById);

            var newRentedByDelete = new RentedByDeleteViewModel()
            {
                RentedById = selectedRentedBy.RentedById,
                CustomerNaam = _context.FindCustomerById(selectedRentedBy.CustomerId).Naam,
                MovieNaam = _context.FindMovieByID(selectedRentedBy.MovieId).Tittel,

            };

            return View(newRentedByDelete);
        }
        [HttpPost]
        public IActionResult DeleteConfirm(RentedByDeleteViewModel newRentedByDelete)
        {
           
            var selectedRentedBy = _context.FindRentedById(newRentedByDelete.RentedById);

            if (selectedRentedBy == null) return new NotFoundResult();

            _context.RemoveRentedBy(selectedRentedBy);

            return RedirectToAction("Index");
        }
    }
}
