﻿namespace MovieWeb.Models
{
    public class CustomerDetailViewModel
    {       
        public string Naam { get; set; }
        public string Voornaam { get; set; }
    }
}
