﻿using MovieWeb.Domain;

namespace MovieWeb.Models
{
    public class MovieDetailViewModel
    {
        public Guid MovieId { get; set; }

        public string? Tittel { get; set; }

        public int Duurtijd { get; set; }

        public DateTime Release { get; set; }
        public MovieImg? MovieImg { get; set; }

        public List<MovieImg>? MovieScreenshots { get; set; } 


    }
}
