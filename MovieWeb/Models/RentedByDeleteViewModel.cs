﻿namespace MovieWeb.Models
{
    public class RentedByDeleteViewModel
    {
        public Guid RentedById { get; set; }
        public string CustomerNaam { get; set; }
        public string MovieNaam { get; set; }
    }
}
