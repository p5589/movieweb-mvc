﻿namespace MovieWeb.Models
{
    public class RentedByEditViewModel
    {
        public Guid RentedById { get; set; }
        public Guid CustomerId { get; set; }
        public Guid MovieId { get; set; }
    }
}

