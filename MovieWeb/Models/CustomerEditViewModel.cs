﻿namespace MovieWeb.Models
{
    public class CustomerEditViewModel
    {
        public Guid CustomerId { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }
    }
}
