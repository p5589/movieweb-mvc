﻿using MovieWeb.Domain;

namespace MovieWeb.Models
{
    public class MovieListViewModel
    {
        public Guid MovieId { get; set; }
        public string? Tittel { get; set; }

        public DateTime Release { get; set; }
        public int Duurtijd { get; set; }
      
        public MovieImg MovieImg { get; set; }
    }
}
