﻿namespace MovieWeb.Models
{
    public class UitgaveDeleteViewModel
    {
        public Guid ID { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
