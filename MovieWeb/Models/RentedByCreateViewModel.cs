﻿namespace MovieWeb.Models
{
    public class RentedByCreateViewModel
    {
        public Guid RentedById { get; set; }
        public Guid CustomerId { get; set; }
        public Guid MovieId { get; set; }
    }
}
