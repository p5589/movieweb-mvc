﻿using MovieWeb.Domain;

namespace MovieWeb.Models
{
    public class MovieNightCreateViewModel
    {
        public Guid MovieNightId { get; set; }
        public string? Naam { get; set; }
        public string? Omschrijving { get; set; }

        public Movie? MovieToAdd { get; set; }
        public Customer? CustomerToAdd { get; set; }
        public List<Customer>? AllCustomers { get; set; }
        public List<Movie>? AllMovies { get; set; }
    }
}
