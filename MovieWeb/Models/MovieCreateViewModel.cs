﻿
using MovieWeb.Domain;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MovieWeb.Models
{
    public class MovieCreateViewModel
    {
        [DisplayName("ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID verplicht")]
        public Guid MovieId { get; set; }

        [MinLength(1, ErrorMessage = "min 1 teken")]
        [MaxLength(100, ErrorMessage = "max 100 tekens")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Wel iets ingeven he")]
        public string? Tittel { get; set; }


        
        [Required(ErrorMessage = "Wel iets ingeven he")]
        public int Duurtijd { get; set; }

        [Required(ErrorMessage = "Wel iets ingeven he")]
        [Range(typeof(DateTime), "01/01/1920", "01/01/2100", ErrorMessage = "Tussen 1920 en 2100")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0: yyyy-MM-dd}")]
        public DateTime Release { get; set; }

        public MovieImg? MovieImg { get; set; }

        public List<MovieImg>? MovieScreenshots { get; set; }


    }
}
