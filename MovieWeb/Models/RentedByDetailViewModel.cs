﻿using MovieWeb.Domain;

namespace MovieWeb.Models
{
    public class RentedByDetailViewModel
    {
        public Guid RentedById { get; set; }
        public Customer Customer { get; set; }
        public Movie Movie { get; set; }
    }
}
