﻿using System.ComponentModel.DataAnnotations;

namespace MovieWeb.Domain
{
    public class Uitgave
    {
        public Guid ID { get; set; }    
        public string Description { get; set; }
        public DateTime Date { get; set; }
        [Range (0, (double)decimal.MaxValue)]   
        public decimal Amount { get; set; }
    }
}
