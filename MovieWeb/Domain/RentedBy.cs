﻿namespace MovieWeb.Domain
{
    public class RentedBy
    {
        public Guid RentedById { get; set; }
        public Guid CustomerId { get; set; }
        public Guid MovieId { get; set; }
    }
}

