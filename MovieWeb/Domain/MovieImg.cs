﻿namespace MovieWeb.Domain
{
    public class MovieImg
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
    }
}
