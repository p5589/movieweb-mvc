﻿namespace MovieWeb.Domain
{
    public class MovieNight
    {
        public Guid MovieNightId { get; set; }
        public string? Naam { get; set; }
        public string? Omschrijving { get; set; }        
        public List<Customer>? AllCustomers { get; set; }
        public List<Movie>? AllMovies { get; set; }

    }
}
