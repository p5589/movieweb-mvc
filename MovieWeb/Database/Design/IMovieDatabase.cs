﻿using MovieWeb.Domain;

namespace MovieWeb.Database.Design
{
    public interface IMovieDatabase
    {
        List<Movie> GetMovies();

        Movie FindMovieByID(Guid ID);

        void AddMovie(Movie movie);

        void RemoveMovie(Movie movie);

        void UpdateMovie(Movie movie);

        void AddMovieImg(MovieImg movieimg);

        void RemoveMovieImg(Movie movieimg);

        void UpdateMovieImg(Movie movieimg);

        List<MovieImg> GetMovieImg();

        List<Customer> GetCustomer();

        Customer FindCustomerById(Guid id);
        void AddCustomer(Customer customer);
        void RemoveCustomer(Customer customer);
        void UpdateCustomer(Customer customer);


        List<RentedBy> GetRentedBy();

        RentedBy FindRentedById(Guid id);
        void AddRentedBy(RentedBy rentedBy);
        void RemoveRentedBy(RentedBy rentedBy);
        void UpdateRentedBy(RentedBy rentedBy);


        List<MovieNight> GetMovieNight();

        MovieNight FindMovieNightById(Guid id);
        void AddMovieNight(MovieNight movieNight);
        void RemoveMovieNight(MovieNight movieNight);
        void UpdateMovieNight(MovieNight movieNight);

    }
}
