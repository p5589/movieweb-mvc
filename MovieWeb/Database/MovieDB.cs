﻿using MovieWeb.Database.Design;
using MovieWeb.Domain;

namespace MovieWeb.Database
{
    public class MovieDB : IMovieDatabase
    {

        private readonly MovieDBContext _dbContext;
        private List<Movie> _movies;

        public MovieDB(MovieDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void AddMovie(Movie movie)
        {           
            _dbContext.Movies.Add(movie);           
            _dbContext.SaveChanges();
        }

        public void AddMovieImg(MovieImg movieimg)
        {
            
            _dbContext.MovieImg.Add(movieimg);
            
        }

        public Movie FindMovieByID(Guid ID)
        {
            return _dbContext.Movies.SingleOrDefault(movie => movie.MovieId == ID);
        }

        public List<Movie> GetMovies()
        {                    
            return _dbContext.Movies.ToList();
        }

        public void RemoveMovie(Movie movie)
        {
            _dbContext.Movies.Remove(_dbContext.Movies.FirstOrDefault(x => x.MovieId == movie.MovieId));
            //_dbContext.MovieImg.Remove(_dbContext.MovieImg.FirstOrDefault(x => x.Id == movie.MovieImg.Id));
            _dbContext.SaveChanges();
        }

        public void RemoveMovieImg(Movie movieimg)
        {
            //TO DO
        }

        public List<MovieImg> GetMovieImg()
        {
            return _dbContext.MovieImg.ToList();
        }

        public void UpdateMovie(Movie movie)
        {
            _dbContext.Movies.Remove(_dbContext.Movies.FirstOrDefault(x => x.MovieId == movie.MovieId));
            _dbContext.Movies.Add(movie);
            //_dbContext.MovieImg.Update(movie.MovieImg);
            _dbContext.SaveChanges();
        }
        
        public void UpdateMovieImg(Movie movieimg)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomer()
        {
            return _dbContext.Customers.ToList(); ;
        }

        public Customer FindCustomerById(Guid id)
        {
            return _dbContext.Customers.SingleOrDefault(customer => customer.CustomerId.Equals(id));
        }

        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
            _dbContext.SaveChanges();
        }

        public void RemoveCustomer(Customer customer)
        {
            _dbContext.Customers.Remove(customer);
            _dbContext.SaveChanges();
        }

        public void UpdateCustomer(Customer customer)
        {
            _dbContext.Customers.Update(customer);
            _dbContext.SaveChanges();
        }

        public List<RentedBy> GetRentedBy()
        {
            return _dbContext.RentedBy.ToList();
        }

        public RentedBy FindRentedById(Guid id)
        {
            return _dbContext.RentedBy.SingleOrDefault(rentedBy => rentedBy.RentedById.Equals(id));
        }

        public void AddRentedBy(RentedBy rentedBy)
        {
            _dbContext.RentedBy.Add(rentedBy);
            _dbContext.SaveChanges();

        }

        public void RemoveRentedBy(RentedBy rentedBy)
        {
            _dbContext.RentedBy.Remove(rentedBy);
            _dbContext.SaveChanges();

        }

        public void UpdateRentedBy(RentedBy rentedBy)
        {
            _dbContext.RentedBy.Update(rentedBy);
            _dbContext.SaveChanges();

        }

        public List<MovieNight> GetMovieNight()
        {
            return _dbContext.MovieNights.ToList();
        }

        public MovieNight FindMovieNightById(Guid id)
        {
            return _dbContext.MovieNights.SingleOrDefault(movieNight => movieNight.MovieNightId.Equals(id));
        }

        public void AddMovieNight(MovieNight movieNight)
        {
            _dbContext.MovieNights.Add(movieNight);
            _dbContext.SaveChanges();

        }

        public void RemoveMovieNight(MovieNight movieNight)
        {
            _dbContext.MovieNights.Remove(movieNight);
            _dbContext.SaveChanges();

        }

        public void UpdateMovieNight(MovieNight movieNight)
        {
            _dbContext.MovieNights.Update(movieNight);
            
            _dbContext.SaveChanges();

        }
    }
}
