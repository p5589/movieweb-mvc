﻿using Microsoft.EntityFrameworkCore;
using MovieWeb.Domain;

namespace MovieWeb.Database
    
{
    public class MovieDBContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieImg> MovieImg { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<RentedBy> RentedBy { get; set; }

        public DbSet<MovieNight> MovieNights { get; set; }

        public MovieDBContext(DbContextOptions<MovieDBContext> options) : base(options)
        {

        }
    }
}
